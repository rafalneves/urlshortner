exports.newCon = function(callback){
	var db = mongoose.connection;
	db.on('error', console.error.bind(console, 'connection error:'));
	db.once('open', function(){
		callback(db);
	});
	mongoose.connect('mongodb://localhost/urlshortner');
}