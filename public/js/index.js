$(document).ready(function(e){
	checkHash(window.location.hash);
	
	$("body").on("click", "input#url", function(e){
		e.stopPropagation();
		$(this).val("http://");
		
		$("body").one("click", function(){
			if($("input#url").val() == "http://")
				$("input#url").val('');
		});
	});
	
	$("form#url").on("submit", function(e){
		e.preventDefault();
		
		$.ajax({
			url: "/urls/url/short",
			type: "post",
			data: $("form#url").serialize(),
			success: function(response, textStatus, jqXHR){
				$("div.url-form").remove();

				if(response.url)
					$("section.content").append(
						'<div class="url-view">' +
							'<div class="url">' +
								'<a class="url" href="' + response.url + '" target="_blank">http://urls.mixingpixels.com/' + response.shortUrl + '</a>' +
							'</div>' +
						'</div>');
				else
					$("section.content").append(
						'<div class="url-view">' +
							'<span>Nothing to show...</span>' +
						'</div>');
						
				window.location.hash = "#Done=" + response.shortUrl;
			
			},
			error: function(jqXHR, textStatus, errorThrown){},
			complete: function(){}
		});
	});
	
	$(window).on("hashchange", function(e){
		checkHash(window.location.hash);
	});
});
function checkHash(hash){
	switch(hash.split('=')[0]){
		case "#404":
			$("div.url-form").remove();
			$("div.url-view").remove();
			$("section.content").append('<div class="error404">URL Not Found</div>');

			break;
		case "#Done":
			if(!$("div.url-view").length)
				$.ajax({
					url: "/url/data/" + hash.split('=')[1],
					type: "get",
					contentType: "application/json",
					dataType: "json",
					success: function(response, textStatus, jqXHR){
						$("div.url-form").remove();
						
						if(response.url)
							$("section.content").append(
								'<div class="url-view">' +
									'<div class="url">' +
										'<a class="url" href="' + response.url + '" target="_blank">http://urls.mixingpixels.com/' + response.shortUrl + '</a>' +
									'</div>' +
								'</div>');
						else
							window.location.hash = '';
					},
					error: function(jqXHR, textStatus, errorThrown){},
					complete: function(){}
				});
				
			break;
		default:
			$("div.url-view").remove();
			$("section.content").append(
				'<div class="url-form">' +
					'<form id="url">' +
						' <input id="url" type="url" class="url-form url-short" name="url" placeholder="Enter your big url..." required />' +
						'<button class="url-form url-short-button" type="submit">Short Me!</button>' +
					'</form>' +
				'</div>');

			break;
	}
}