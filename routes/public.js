var fs = require("fs");
var url = require("url");

exports.getPublicResources = function(request, response){
	var urlVariables = url.parse(request.url).pathname;
	
	if(/\.(inc)$/.test(urlVariables))
		getResourceFromDisc(urlVariables, request, response, "text/html");
	else
		if(/\.(html)$/.test(urlVariables))
			getResourceFromDisc(urlVariables, request, response, "text/html");
		else
			if(/\.(htm)$/.test(urlVariables))
				getResourceFromDisc(urlVariables, request, response, "text/html");
			else
				if(/\.(jpeg)$/.test(urlVariables))
					getResourceFromDisc(urlVariables, request, response, "image/jpeg");
				else
					if(/\.(jpg)$/.test(urlVariables))
						getResourceFromDisc(urlVariables, request, response, "image/jpeg");
					else
						if(/\.(png)$/.test(urlVariables))
							getResourceFromDisc(urlVariables, request, response, "image/png");
						else
							if(/\.(gif)$/.test(urlVariables))
								getResourceFromDisc(urlVariables, request, response, "image/gif");
							else
								if(/\.(js)$/.test(urlVariables))
									getResourceFromDisc(urlVariables, request, response, "text/javascript");
								else
									if (/\.(css)$/.test(urlVariables))
										getResourceFromDisc(urlVariables, request, response, "text/css");
									else
										if (/\.(appcache)$/.test(urlVariables))
											getResourceFromDisc(urlVariables, request, response, "text/cache-manifest");
										else{
											response.writeHead(404, {"Content-Type": "text/plain"});
											response.end();
										}
}
function getResourceFromDisc(urlVariables, request, response, contentType){
	fs.readFile(__dirname + "/.." + urlVariables, function (error, data){
		if(error)
			console.log(error);
		else{
			response.writeHead(200, {"Content-Type": contentType});
			response.write(data);
			response.end();
		}
	});
}