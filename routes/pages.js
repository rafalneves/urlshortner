var fs = require('fs');

exports.getHomePage = function(request, response){
	fs.readFile('views/index.html', function(error, data){
		if(error)
			console.log(error);
		else{
			response.writeHead(200, {'Content-Type': 'text/html'});
			response.write(data);  
			response.end();
		}
	});
}