var urls = require("../lib/urls.js");

exports.newUrl = function(request, response){
	//if needed -> permissions check here
	urls.newUrl(request, response);
}
exports.redirect = function(request, response){
	//if needed -> permissions check here
	urls.redirect(request, response);
}
exports.getURLData = function(request, response){
	//if needed -> permissions check here
	urls.getURLData(request, response);
}