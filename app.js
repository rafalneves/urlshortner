var express = require('express');
var bodyParser = require('body-parser')
GLOBAL.mongoose = require('mongoose');

var expressApp = express();
expressApp.use(bodyParser());

var publicRoutes = require('./routes/public.js');
var pagesRoutes = require('./routes/pages.js');
var urlsRoutes = require('./routes/urls.js');

expressApp.get('/public/*', function(request, response){
	publicRoutes.getPublicResources(request, response);
});
expressApp.get('/', function(request, response){
	pagesRoutes.getHomePage(request, response);
});
expressApp.get('/url/data/:shorturl', function(request, response){
	urlsRoutes.getURLData(request, response);
});
expressApp.get('/:shorturl', function(request, response){
	urlsRoutes.redirect(request, response);
});

expressApp.post('/urls/url/short', function(request, response){
	urlsRoutes.newUrl(request, response);
});

expressApp.listen(2000);