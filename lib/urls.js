var mongoDB = require("../utils/mongoDB.js");
var urlSchema = mongoose.Schema({
	url: String,
	shortUrl: String
});
var Url = mongoose.model('Url', urlSchema);

var shortner = require("./shortner.js");

exports.newUrl = function(request, response){
	mongoDB.newCon(function(db){
		Url.find({ url: request.body.url }, function(error, result){
			if(error){
				mongoose.disconnect();
				
				console.log(console.error(error));
			}else
				if(result && result.length > 0){
					mongoose.disconnect();

					response.writeHead(200, {'Content-Type': 'application/json'});
					response.end(JSON.stringify(result[0]));
				}else
					Url.count(function(error, count){
						if(error){
							mongoose.disconnect();
							
							console.log(console.error(error));
						}else{
							var short = shortner.encode(count);
							
							var newUrl = new Url({
								url: request.body.url,
								shortUrl: short
							});
							
							newUrl.save(function(error){
								mongoose.disconnect();
								
								if(error)
									console.log(console.error(error));
								else{
									response.writeHead(201, {'Content-Type': 'application/json'});
									response.end(JSON.stringify(newUrl));
								}
							});
						}
					});
		});
	});
}
exports.redirect = function(request, response){
	mongoDB.newCon(function(db){
		Url.find({ shortUrl: request.params.shorturl }, function(error, result){
			mongoose.disconnect();
			
			if(error)				
				console.log(console.error(error));
			else
				if(result && result.length > 0){
					response.writeHead(302, { "Location": result[0].url });
					response.end();
				}else{
					response.writeHead(302, { "Location": "/#404" });
					response.end();
				}
		});
	});
}
exports.getURLData = function(request, response){
	mongoDB.newCon(function(db){
		Url.find({ shortUrl: request.params.shorturl }, function(error, result){
			mongoose.disconnect();
			
			if(error)				
				console.log(console.error(error));
			else
				if(result && result.length > 0){
					mongoose.disconnect();

					response.writeHead(200, {'Content-Type': 'application/json'});
					response.end(JSON.stringify(result[0]));
				}else
					console.log(console.error("UnknownError"));
		});
	});
}