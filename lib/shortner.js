const ALPHABET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".split("")

exports.encode = function(i){
	if(i == 0)
		return ALPHABET[0];

	var string = '';
	while(i > 0){
		string += ALPHABET[i % ALPHABET.length]
		i = parseInt(i / ALPHABET.length, 10);
	}
	return string.split("").reverse().join("")
}
exports.decode = function(string){
	i = 0
	for(var y = 0; y < string.length; i++)
		i = i * string.length + ALPHABET.indexOf(string[y]);
	
	return i;
}